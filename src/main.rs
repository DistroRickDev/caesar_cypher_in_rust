
static  _ALPHABET: [char; 52]   = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 
'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q' ,'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];

struct _Message{
    message: String,
    shift: i8
}

impl _Message{
    fn _print_message(&self){
           println!("{}", self.message);
    }
}

fn _encode_char(mut elem:char, num:i8) -> char{
    for runs in 0..52{
        if elem == _ALPHABET[runs] {
            if runs as i8 + num >= 52
            {
                elem = _ALPHABET[((runs as i8 + num) - 52) as usize];
                return elem;
            }
            else{
                elem = _ALPHABET[(runs as i8 + num)  as usize];
                return elem;
            }
        }
    }
    return  elem;
}

fn _enconde_msg(msg: &mut _Message){
    let mut new_message = String::new();
    for mut element in msg.message.chars(){
        element = _encode_char(element, msg.shift);
        new_message.push(element);
    }
    msg.message = new_message;
}

fn _decode_char(mut elem:char, num:i8) -> char{
    for runs in 0..52{
        if elem == _ALPHABET[runs] {
            if runs as i8 - num <= 0
            {
                elem = _ALPHABET[((runs as i8 - num) + 52) as usize];
                return elem;
            }
            else{
                elem = _ALPHABET[(runs as i8 - num)  as usize];
                return elem;
            }
        }
    }
    return  elem;
}

fn _decode_msg(msg: &mut _Message){
    let mut new_message = String::new();
    for mut element in msg.message.chars(){
        element = _decode_char(element, msg.shift);
        new_message.push(element);
    }
    msg.message = new_message;
}

fn main()
{
    let mut new_message = _Message{
        message: "'What' ain't no country I ever heard of, do they speak English in 'What'?".to_string(),
        shift: 3
    };
    _enconde_msg(&mut new_message);
    new_message._print_message();
    _decode_msg(&mut new_message);
    new_message._print_message();
}